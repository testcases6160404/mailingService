from django.db import models
from django.db.models import Count, Q, QuerySet

from notifications.db.models import Client, Mailing, Message


class ClientCRUD:
    @staticmethod
    def get_all_clients() -> QuerySet[Client]:
        return Client.objects.all()

    @staticmethod
    def filter_on_tag_and_mobile_operator_code(tag: str, mobile_operator_code: str) -> QuerySet[Client]:
        return Client.objects.filter(Q(tag=f"{tag}") & Q(mobile_operator_code=f"{mobile_operator_code}"))


class MailingCRUD:
    @staticmethod
    def get_by_id(id_: int) -> Mailing | None:
        try:
            mailing = Mailing.objects.get(pk=id_)
        except Mailing.DoesNotExist:
            return None
        else:
            return mailing

    @staticmethod
    def create(**kwargs) -> Mailing:
        mailing = Mailing.objects.create(**kwargs)
        return mailing

    @staticmethod
    def get_all_mailings() -> QuerySet[Mailing]:
        return Mailing.objects.all()

    @staticmethod
    def get_full_stats():
        stats = Mailing.objects.annotate(
            total_messages=Count("messages"),
            sent_messages=Count("messages", filter=models.Q(messages__status=Message.SENT)),
            failed_messages=Count("messages", filter=models.Q(messages__status=Message.FAILED)),
        ).values("id", "message", "total_messages", "sent_messages", "failed_messages")
        return stats

    @staticmethod
    def get_detail_stats(id_: int):
        stats = Mailing.objects.annotate(
            total_messages=Count("messages"),
            sent_messages=Count("messages", filter=models.Q(messages__status=Message.SENT)),
            failed_messages=Count("messages", filter=models.Q(messages__status=Message.FAILED)),
        ).get(pk=id_)
        return stats


class MessageCRUD:
    @staticmethod
    def create(**kwargs) -> Message:
        mailing = Message.objects.create(**kwargs)
        return mailing

    @staticmethod
    def filter_by_mailing_id(mailing_id: int) -> QuerySet[Message]:
        return Message.objects.filter(mailing_id=mailing_id)
