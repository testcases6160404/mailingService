import pytz  # type: ignore
from django.db import models


class Client(models.Model):
    _TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    mobile = models.CharField("Номер телефона", max_length=11, unique=True)

    mobile_operator_code = models.CharField("Код мобильного оператора")
    tag = models.CharField("Метка", null=True, blank=True)
    timezone = models.CharField("Часовой пояс", max_length=32, choices=_TIMEZONES, default="UTC")

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    def __str__(self):
        return self.mobile


class Mailing(models.Model):
    message = models.TextField("Текст")
    filter = models.JSONField("Фильтр клиента")

    started_at = models.DateTimeField("Начало")
    ended_at = models.DateTimeField("Окончание")

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"

    def __str__(self):
        return self.message


class Message(models.Model):
    SENT = "sent"
    FAILED = "failed"

    STATUS_CHOICES = [(SENT, "Sent"), (FAILED, "Failed")]

    created_at = models.DateTimeField("Время отправки", auto_now_add=True)
    status = models.CharField("Статус отправки", choices=STATUS_CHOICES)

    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name="messages")
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="clients")

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

    def __str__(self):
        return f"Клиент: {self.client}. Статус: {self.status}"
