from django.urls import path
from rest_framework.routers import SimpleRouter

from notifications.api import views

urlpatterns = [
    path("messages/stats-by-mailing/<int:pk>/", views.DetailMessageStatsAPIView.as_view(), name="detail_stats")
]

router = SimpleRouter()
router.register("clients", views.ClientViewSet)
router.register("mailings", views.MailingViewSet)

urlpatterns += router.urls
