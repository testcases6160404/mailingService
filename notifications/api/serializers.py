from rest_framework import serializers

from notifications.db.models import Client, Mailing, Message
from notifications.services.validators import phone_number_validator


class ClientSerializer(serializers.ModelSerializer):
    mobile = serializers.CharField(required=True, max_length=11, validators=[phone_number_validator])

    class Meta:
        model = Client
        fields = "__all__"


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = "__all__"


class MailingStatsSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    message = serializers.CharField()
    total_messages = serializers.IntegerField()
    sent_messages = serializers.IntegerField()
    failed_messages = serializers.IntegerField()

    class Meta:
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = "__all__"
