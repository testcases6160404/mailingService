from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from notifications.api.serializers import (
    ClientSerializer,
    MailingSerializer,
    MailingStatsSerializer,
    MessageSerializer,
)
from notifications.db.crud import ClientCRUD, MailingCRUD
from notifications.services.tasks import execute_mailing


class ClientViewSet(ModelViewSet):
    http_method_names = ["post", "put", "patch", "delete"]
    serializer_class = ClientSerializer
    queryset = ClientCRUD.get_all_clients()


class MailingViewSet(ModelViewSet):
    serializer_class = MailingSerializer
    queryset = MailingCRUD.get_all_mailings()

    @extend_schema(
        description="Получение общей статистики по созданным рассылкам",
        responses={200: MailingStatsSerializer(many=True)},
    )
    @action(detail=False, url_path="stats", name="full_stats")
    def full_stats(self, request, *args, **kwargs):
        qs = MailingCRUD.get_full_stats()
        serializer = MailingStatsSerializer(qs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        message = serializer.data.get("message")
        filter_ = serializer.data.get("filter")
        started_at = serializer.data.get("started_at")
        ended_at = serializer.data.get("ended_at")

        mailing = MailingCRUD.create(message=message, filter=filter_, started_at=started_at, ended_at=ended_at)
        execute_mailing.apply_async(args=[mailing.pk], eta=mailing.started_at)

        serializer = MailingSerializer(mailing)
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)


class DetailMessageStatsAPIView(APIView):
    serializer_class = MessageSerializer
    queryset = MailingCRUD.get_all_mailings()

    @extend_schema(
        description="Получение детальной статистики отправленных сообщений по конкретной рассылке",
        responses={200: MailingStatsSerializer},
    )
    def get(self, request, *args, **kwargs):
        stats = MailingCRUD.get_detail_stats(kwargs.get("pk"))
        serializer = MailingStatsSerializer(stats)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
