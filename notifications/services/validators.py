from django.core.exceptions import ValidationError


def phone_number_validator(value: str) -> None:
    if not value.startswith("7"):
        raise ValidationError("Номер мобильного телефона должен начинаться с цифры 7.")
    if len(value) != 11:
        raise ValidationError("Ожидаемая длина номера мобильного телефона - 11.")
