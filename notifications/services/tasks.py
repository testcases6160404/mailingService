from celery import shared_task
from django.conf import settings
from django.utils import timezone
from requests import RequestException  # type: ignore

from notifications.db.crud import ClientCRUD, MailingCRUD, MessageCRUD
from notifications.db.models import Message
from notifications.services.mailing import send_msg_to_external_api


@shared_task
def execute_mailing(mailing_id: int) -> None:
    mailing = MailingCRUD.get_by_id(mailing_id)
    if not mailing:
        return None

    clients = ClientCRUD.filter_on_tag_and_mobile_operator_code(
        tag=mailing.filter.get("tag"), mobile_operator_code=mailing.filter.get("mobile_operator_code")
    )

    for client in clients:
        current_datetime = timezone.now()
        # If didn't have enough time to send messages
        if not mailing.started_at < current_datetime < mailing.ended_at:
            break
        else:
            data_to_send = {"id": client.pk, "phone": client.mobile, "text": mailing.message}

            time_sent = timezone.now()
            try:
                send_msg_to_external_api(f"{settings.API_URL}{mailing.pk}", data_to_send)
            except RequestException:
                MessageCRUD.create(created_at=time_sent, status=Message.FAILED, mailing=mailing, client=client)
            else:
                MessageCRUD.create(created_at=time_sent, status=Message.SENT, mailing=mailing, client=client)
