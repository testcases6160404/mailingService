import logging

import requests  # type: ignore
from requests import ConnectTimeout, ReadTimeout, RequestException

from core.settings import API_KEY

logger = logging.getLogger("main")


def send_msg_to_external_api(url: str, data: dict) -> None:
    try:
        response = requests.post(url, data=data, headers={"Authorization": f"Bearer {API_KEY}"}, timeout=1.5)
        response.raise_for_status()
    except ConnectTimeout as err:
        logger.info(err)
        raise
    except ReadTimeout as err:
        logger.info(err)
        raise
    except RequestException as err:  # Base Exception in requests
        logger.info(err)
        raise
    else:
        logger.info(response)
