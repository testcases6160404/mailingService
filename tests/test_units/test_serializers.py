from notifications.api.serializers import ClientSerializer


def test_serializer_mobile_correct():
    serializer = ClientSerializer(data={"mobile": "78005553535", "mobile_operator_code": "100", "tag": "Tag"})
    assert serializer.is_valid() is True


def test_serializer_mobile_does_not_correct():
    serializer = ClientSerializer(data={"mobile": "88005553535", "mobile_operator_code": "100", "tag": "Tag"})
    assert serializer.is_valid() is False
