import pytest
from pytest_mock import MockFixture
from requests import RequestException  # type: ignore

from notifications.db.models import Client, Mailing, Message
from notifications.services.tasks import execute_mailing


@pytest.mark.django_db
def test_execute_mailing_no_exception(mailing: Mailing, client: Client, mocker: MockFixture, celery_fixture):
    mocker.patch(
        "notifications.services.tasks.send_msg_to_external_api", side_effect=None
    )  # mocking external API call

    execute_mailing(mailing_id=mailing.pk)
    assert Message.objects.get(client=client).status == Message.SENT


@pytest.mark.django_db
def test_execute_mailing_with_exception(mailing: Mailing, client: Client, mocker: MockFixture, celery_fixture):
    mocker.patch(
        "notifications.services.mailing.send_msg_to_external_api", side_effect=RequestException
    )  # mocking external API call
    execute_mailing(mailing_id=mailing.pk)
    assert Message.objects.get(client=client).status == Message.FAILED
