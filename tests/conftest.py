import datetime

import pytest
from django.utils import timezone

import celery_app
from notifications.db.models import Client, Mailing


@pytest.fixture(scope="module")
def celery_fixture(request):
    celery_app.app.conf.update(CELERY_ALWAYS_EAGER=True)
    return celery_app


@pytest.fixture
def mailing() -> Mailing:
    mailing = Mailing.objects.create(
        started_at=timezone.now(),
        ended_at=timezone.now() + datetime.timedelta(hours=1),
        message="Message",
        filter={"tag": "Tag", "mobile_operator_code": "200"},
    )
    return mailing


@pytest.fixture
def client() -> Client:
    client = Client.objects.create(mobile="7700503500", mobile_operator_code="200", tag="Tag")
    return client
