import datetime

import pytest
from django.urls import reverse
from django.utils import timezone
from pytest_mock import MockFixture
from rest_framework import status
from rest_framework.test import APIClient


class TestClientAPI:
    @pytest.mark.django_db
    def test_create_client(self, api_client):
        url = reverse("client-list")
        response = api_client.post(url, data={"mobile": "78005553535", "mobile_operator_code": "100", "tag": "Tag"})
        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.django_db
    def test_create_client_mobile_not_correct(self, api_client):
        url = reverse("client-list")
        response = api_client.post(url, data={"mobile": "88005553535", "mobile_operator_code": "100", "tag": "Tag"})
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.django_db
    def test_delete_client(self, api_client, client):
        url = reverse("client-detail", args=[client.pk])
        response = api_client.delete(url)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    @pytest.mark.django_db
    def test_update_client(self, api_client, client):
        url = reverse("client-detail", args=[client.pk])
        response = api_client.patch(url, data={"mobile": "77005553535"})
        assert response.status_code == status.HTTP_200_OK


class TestMailingAPI:
    @pytest.mark.django_db
    def test_create_mailing(self, api_client: APIClient, mocker: MockFixture, celery_fixture):
        mocker.patch(
            "notifications.services.tasks.send_msg_to_external_api", side_effect=None
        )  # mocking external API call

        url = reverse("mailing-list")
        response = api_client.post(
            url,
            data={
                "started_at": timezone.now(),
                "ended_at": timezone.now() + datetime.timedelta(hours=2),
                "message": "message",
                "filter": '{"tag": "10"}',
            },
        )

        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.django_db
    def test_delete_mailing(self, api_client, mailing):
        url = reverse("mailing-detail", args=[mailing.pk])
        response = api_client.delete(url)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    @pytest.mark.django_db
    def test_update_mailing(self, api_client, mailing):
        url = reverse("mailing-detail", args=[mailing.pk])
        response = api_client.patch(url, data={"message": "New Message..."})
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_get_mailing_full_stats(self, api_client, mailing):
        url = reverse("mailing-full-stats")
        response = api_client.get(url)
        assert response.status_code == status.HTTP_200_OK


class TestMessageAPI:
    @pytest.mark.django_db
    def test_get_message_stats(self, api_client, mailing):
        url = reverse("detail_stats", args=[mailing.pk])
        response = api_client.get(url)
        assert response.status_code == status.HTTP_200_OK
